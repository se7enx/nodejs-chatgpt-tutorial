#Vehicles owned by the Broadfoot family

##My Car
The Honda CR-V is a versatile and practical family SUV designed to meet the needs of modern families. Equipped with a 2.0-liter turbocharged inline-four engine, the CR-V delivers a robust 190 horsepower and 179 lb-ft of torque. This powertrain is mated to a smooth and efficient continuously variable transmission (CVT), ensuring seamless acceleration and optimal fuel efficiency. The CR-V's spacious interior comfortably seats five passengers, and its advanced safety features, including adaptive cruise control and lane-keeping assist, make it an ideal choice for families seeking a reliable and safe vehicle for their daily adventures. Whether navigating urban streets or embarking on a road trip, the Honda CR-V offers a balanced blend of performance, comfort, and practicality for today's families.

##My Boat
The Chaparral 237 SSX is a stylish and versatile recreational boat perfect for family outings and watersports. This bowrider boat features a powerful MerCruiser 6.2L V8 engine, generating an impressive 350 horsepower, providing exhilarating performance on the water. With a length of 23 feet and 8 inches, the 237 SSX offers ample space for family and friends to relax and enjoy the sun.

Equipped with Chaparral's signature Extended V-Plane hull, the boat delivers a smooth and stable ride, cutting through waves with ease. The spacious cockpit boasts comfortable seating and well-designed amenities, including a swim platform, a sun lounge, and a convenient helm with advanced navigation and entertainment systems.

Whether you're towing thrill-seekers on water skis, wakeboards, or tubes, the Chaparral 237 SSX's powerful engine and well-crafted design ensure an exciting and enjoyable experience for the whole family. Whether exploring calm lakes or cruising along coastal waters, this recreational boat combines performance, comfort, and style to create lasting memories on the water.