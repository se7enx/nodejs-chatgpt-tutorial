import { Configuration, OpenAIApi } from "openai";
import readline from "readline";
import natural from "natural";
import fs from "fs";
import { PineconeClient } from "@pinecone-database/pinecone";
import { OpenAIEmbeddings } from 'langchain/embeddings/openai';
import dotenv from "dotenv";

// Load environment variables from .env file
dotenv.config();

const pineconoe_apiKey = process.env.PINECONE_API_KEY;
const openai_key = process.env.OPENAI_KEY;

const pinecone = new PineconeClient();

const indexName = "cc-test";

const embeddings = new OpenAIEmbeddings({openAIApiKey: openai_key});

await pinecone.init({
  environment: "us-east4-gcp",
  apiKey: apiKey,
});

const index = pinecone.Index(indexName);

const queryResponse = await index.query({
  queryRequest: {
    namespace: "broadfoot-cars",
    topK: 10,
    includeValues: true,
    includeMetadata: true,
    vector: [0.1, 0.2, 0.3, 0.4],
  },
});

console.log(queryResponse);

process.exit(1);

const tokenizer = new natural.SentenceTokenizer();

const configuration = new Configuration({
  apiKey: openai_key,
});

// Load document content
let documentContent;
try {
  documentContent = fs.readFileSync("/Users/dbroadfoot/nodejs-chatgpt-tutorial/test.md", "utf-8");
} catch (error) {
  console.error("Error reading document:", error.message);
  process.exit(1); // Terminate the program
}

// Tokenize document sections
const sections = documentContent.split(/\n(#+ .+)\n/);
const cleanedSections = sections.filter(section => section.trim() !== "");
const tokenizedSections = cleanedSections.map(section => {
  const sentences = tokenizer.tokenize(section);
  return sentences;
});
const allSentences = tokenizedSections.flat();

const openai = new OpenAIApi(configuration);

const userInterface = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

userInterface.prompt();

userInterface.on("line", async (input) => {
  try {
    // Combine user input and context from the document

    const instruction = "You are an assistant whose role is to help respond to questions based on information contained in a specific doument. Limit all answers to infomation gathered from that document. The docuemtn is called 'The Broadfoot Family Vehicles' and has been parsed as follows:";

    const conversation = [
      {
        role: "system",
        content: instruction,
      },

      ...allSentences.map((sentence) => ({
        role: "system",
        content: sentence,
      })),

      // User input
      { role: "user", content: input }

    ];

    // Log the conversation for debugging purposes
    console.log("Conversation:", JSON.stringify(conversation, null, 2));

    const response = await openai.createChatCompletion({
      model: "gpt-3.5-turbo",
      messages: conversation,
    });

    // Log the bot's response
    console.log("Bot Response:", response.data.choices[0].message.content);
  } catch (error) {
    // Handle errors
    console.error("Error from OpenAI API:", error.message, error.response?.data);
  } finally {
    // Display the user prompt
    userInterface.prompt();
  }
});
